import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    title_groups = ["Mr.", "Mrs.", "Miss."]
    results = []
    for title in title_groups:
        median_age = df.loc[df["Name"].str.contains(title, case=False, regex=False)]['Age'].median()
        median_age = round(median_age) if not np.isnan(median_age) else None
        missing_val = df.loc[(df["Name"].str.contains(title,case=False,regex = False))&(df["Age"].isnull())]["Age"].count()
        results.append((title,missing_val,median_age))
    
    
    return results

'''There is issue with finding train.csv!!!!!!'''